# FIXME upload to a more official repository and sign with an existing key
if [ ! -e /etc/apt/sources.list.d/noosfero.list ]; then
  sudo tee /etc/apt/sources.list.d/noosfero.list <<EOF
deb http://people.debian.org/~terceiro/noosfero-wheezy-backports/ ./
EOF
  sudo apt-key add - <<EOF
-----BEGIN PGP PUBLIC KEY BLOCK-----
Version: GnuPG v1

mQINBEo1mTQBEAD29YIKM0hM2IsB+TzBOpQja6h5hJ1gVeP7IWhC8E11jwaaoP1K
SXESKFMVPt0es0aCSDftm5TVTvLl08MG9fZBFT8pERfkWTEWWhY1MJ28sV8PRBHf
nhN0mv5aduvgVx32+aCD0mWhI/3XHObf6c/X9WMwEaH+6A9UFiXRCyflra9YOfHU
inXj5aYllc2UNiNxPhJ5sQTv97hdVbb/dFa+t9A2LFgPADbwUW+ShJN8zGR3XMne
lXTcOgPhBadiNPPi8PztfOgcUk/NqZ5H4i0cXXbZB6leWcGMMMiWNexapgea/Hvp
aXT2kSIug7OySFXM4nhM4xGo426+r9QHx1dzndHP5AW+WRuJmi3hMEdj22ifIFrV
XtwovTb6/R2nG7WLVSHKg1AuFvIfG1RrTaVUVMcXVGX83CB2qKjfMHFRyMKUxoK7
cZZkgD+sst47L+P3MlrRPfuB6selEySIrcwXTxFiuhLf74Zc8AZZeOi5ae2GObGr
b40wIT4HpJZO9M2YfZkMcnFsAqB8D4tPz9OjXiZdqNrXBMKJo0nsOFsYqkOCpnXP
ic4HQvmEaEWzNuPlWFH54AQf3zAWMoawLXOfgAOYAc8gf1dxYOTMJEBIplBBj8ch
E7WeiTv/f9vxuyX4CfTuuX/EEPTvkJ69bcBExtSm+bNs7fgwQwju0sHE7QARAQAB
tC1BbnRvbmlvIFRlcmNlaXJvIDx0ZXJjZWlyb0Bzb2Z0d2FyZWxpdnJlLm9yZz6J
AjoEEwEIACQCGwMFCwkIBwMFFQoJCAsFFgIDAQACHgECF4AFAko1mzkCGQEACgkQ
/A2xu81GC96yww/9HLaTgFTwnKZUhFEgcR9uOyZrb87NvCuXRkGAcy9sr5axSewh
Fibf0I6eFWvq2goOMaPp4rKAFI8UDm/clFUrO62LRKUXy+1/8KWvTJm4c7p7mg9f
Yhs8DPdQAKhl1AOqsT0m93ck+rQcYfAmSkBkX3usu+xEECrVH7oxD0nv3K+UDupT
TXHYYvSBb2xjB8H7MQqEM04utbZb8osRFBKlz7TEbwuxyacvZPEq9qlchStPVMCm
sr/sxeSSZawjKl3oZBG58JPj4+/nLkK+ADVP8E/lEtK45u47576QxgA8YHiNYtFm
lmsrvCyHNaS2g4Pj9gYamYDeFehdH2roQnp28829b/VEs2sUMltj/hZQ58QQNdqe
CpQVuHELD9tIw7CjGwjUYEMD8sow2ebwnl+ZNj5gz4PtOfetTR+0VBJG69kug1GC
lD5SsRrdUp3dSfFAIoMG0LTDnfobjDX/VbJ24hBbCAkdE/1io/57X6GSlDg2tZ4+
gwIexMXzoRSl+to3XQBXSi8CdU99ePlf7MRNIJHpCZnFKZIMVlWpDckqvrGn4Ubv
UfDBnS7K7lBOKxWvOX+uvYxSD0Deo5tbCPKH40pYmDT9R87QQhzI8QnrUUaUEcM0
G28XNcamChJFH/mEU3+Is9fSV4eyoN1sKi0gbvtgha0AZewFbeUkCH5aLae0J0Fu
dG9uaW8gVGVyY2Vpcm8gPHRlcmNlaXJvQGRjYy51ZmJhLmJyPokCNwQTAQgAIQUC
SjWa4AIbAwULCQgHAwUVCgkICwUWAgMBAAIeAQIXgAAKCRD8DbG7zUYL3u4FD/4x
qv8lFOEc8so7Iktp/1b3wLJ6jCyQ1XtgC2Bem91Rda0VGCoTj83nh1NXW2tV1Phi
yIo82LA8/oRqsQnMVZZLe1kRX7vnpLzOjUG17kVOwpt7KwuK2VpXFoSYlntSR8cH
P/vfyhuySN5Gd9RN/liSJLEMUb+r16ibm56mOSWpHFddiQtCGYkqP7Iz1w6L2axq
+d+LPtD4jKVvQHTh9WPMDjZLNn4+glNOCXWj24GjWzdwsJuUJnOybDbgEpNLdpV0
I1Tr52ObTB4V1JmMQQ33pPwe70xeV9FcyIhKJKy+6NE/S7yiJDqYd1a10pDQDNvJ
H2t+d6bxuI6+E3AEhvoOw6mTNrbLWIBciNGEWFSZc18gkmOLEijiu5cmFbvp6btu
6glgPphTJGkGqkS96KKJcohCYtok9s6QFNr4+yb3iXb1nB2j484vCdukjEg0xoxE
4sfhf/QRDuKfl5SWwebIhTxv50pJ+WCdXCTneK19SZO86EaGYInNdjZkEF4dO51O
cLYvbcy+zMMpINdZu+yp2eaaAtJbFPBaMtbY2KJGDp4gzfRiYhyGcujZ3vJ1u5Ch
KWqtPDsIXsr6a5hy9xI0WFeZ2CeHVO1UvW6i7qj+bgSDI9riAuub4ksk1LJvEL08
48yC3EZ5Vdai0PY40lVm7+u7Guq8WJku2LnTx2owUbQrQW50b25pbyBUZXJjZWly
byA8dGVyY2Vpcm9AY29saXZyZS5jb29wLmJyPokCNwQTAQgAIQUCSjWazAIbAwUL
CQgHAwUVCgkICwUWAgMBAAIeAQIXgAAKCRD8DbG7zUYL3o7ND/9yesmfkHbrezjL
nTthjgFizm96wDqCpX9Vif6/YNjorXxy5Yrd+Q3XHrPwkDvJQtrIbuG9IfQDvFRQ
OdvieMquBDbCfIxFVfi8Vbvji6WyR5NU81TbcIw3MC9HXZP9fKJ7ZtFeqjXdjJEs
stfoi4LmNPzPZqAD8DlV3gpTZ2gIPivA4QxxRR+tX3DHktklbLr8cektLzi1LJIH
eXYMpVR9l0TyCmaszQiQOzVBUETmANuSgKOvs9qK80bvjrkuA5R7gx5py/VUOFht
SJzjutL/eVlW/mqJQ/D7l6Je8XhX+niCw0HhbnHMtUruP8pwjcRkdf1b9ikxC1ib
zpcxw0b6hn5tbcFZxcGWKQYv3H37ve4JJ74ZUnQPwbLKGhxXYi5UpNb1D1gePE/+
685bwDYI+4r9NB8Wqm7D+Qthaucmwg44lBscXtqUYZJqKGBR8PNH05j83H9q6FLy
6M0/XYe9dDv6+PJ8QdtRe9ovQ5CibJZV/BERehm4g66k0t6WXofcDuw9mipLz+Os
YUcx5MSnlbCGFtJjDlgNGaFh7S3cnWf1MX8lAJvftuzbtNq5FWKG0YihxhzMOiUf
0BTWrh4DYmf3wVs8EGL6JVhTZK6bJRYQjyUrwQ1LuAJ8C40N0bjAnMEMBi5Sratn
3uZRWXmoqepFn1D4nin/vXxI3p3ZN7QmQW50b25pbyBUZXJjZWlybyA8dGVyY2Vp
cm9AZGViaWFuLm9yZz6JAjcEEwEIACEFAk4ocKwCGwMFCwkIBwMFFQoJCAsFFgID
AQACHgECF4AACgkQ/A2xu81GC94nahAA8/gqJeozduayDxTWF/iNnC8PMOD0Stnn
8GxuoamLmxspmu6etbwSm2FBBrBBZOGOVx6rV6OvtmzYnyIallEqY0lMEi7yySA/
spPvvEihB8KkVj8CW55BE8vaNhynXzneVzHpheV6zoH8o6YzBzg84LB4X3+IG3EZ
52SUVMUoek6ZSUrTXLA0AuJE/LrchzRsVzEQj6Y9h5gpCsj9P/8n+4y403k26tuO
sZWqLv/Sb3byinB0hdrg4U9sm9fk4yDB9fmFWQUaj5nUDxxPgWjdKrvI+y9+d4nP
09Jg0fgG8viiQTer/Kg0n3Pwka7zGEn2o2BvZ/1LQBF0h+0BqNM0uXxoJoGVyOgY
P1CIgfy/LZyl9f9kxDN8IYco0sCAjq8ZYIfjc0TLOI6F3StZZaEXDpXfFGfROh2e
0/1gNEIKicgdLWYQNzcNf3Y7GqJtigRQaGSXpqN2WVL54vTpR55+IPEGgZUNlEU+
cG366pk9gj8gJo3Myhtm8jibXsPfVbVMz7QPJR/DfBte6s3maFdTDM4X8VbuMGRm
PqH/6P7+KNVXdOBBMZMk223sZ2xdFVrrFE0njVSnXq+0kX6qmmG323YBm76XNdBO
4a2s3weSJAboygKJ7uLUGgt2/6FZt2eMlUizta9eGxzkQ0c5e5MVBxHoC0BD1jTu
FkQeKfqTDH60LkFudG9uaW8gVGVyY2Vpcm8gPGFudG9uaW8udGVyY2Vpcm9AbGlu
YXJvLm9yZz6JAjcEEwEIACEFAlBGLZoCGwMFCwkIBwMFFQoJCAsFFgIDAQACHgEC
F4AACgkQ/A2xu81GC96aExAAgz2svW9mADXyLLUEOFMbpNR6G4gtXRTKZNmOsJBe
7DRewKFWdKlXbtxLs77uZPqzX7S/sNFXetOieg1C2MBM1rL5artKpaXrHxWZEyCa
DbWVeDdnCC0/WYn6hqrIyqjAJx9h+e3fpGTqxOuztMcp2uiMijuYVaPyCN0z6Hnj
Y7p/IUTAUtUDU8dLQqzYimKrmsioIEPWlzt44g1MOLt2znQjedMTrDwQOstJj2G7
9v0meFVllTpMBSECKnt1I4SQEk0kq/R3cPqqLBJ75K+DH0fsGlqb6tW0oIB2BZDn
nGnttUphS3xgSd1qy3/wTB047w2i5DqrHfleUgyWOMydSck8PK11i1x9UYdV0137
foVbTnRdY8/uoH32r4SjlRCeQA5VSeBxIq4EDW+G6tGEDawRji4x94TDgXYm54hO
OzFhkL7zi0wtWc1GFQF7vZyLE93hYkhID70MnajqU0umZL+losmBiOvQGkIt9hSC
iL+oTlmRw+SXtu7aPmu7Ue9s1ieaeivTMlFf7OD3tB1m/YiRbQH/XcBMaQp0GELI
QeSzYmmY03E8E/X5IPtF0tRCvfp9GmUn3lfBDWcYhaWy6k3YqvLw8FS+uhmYwfhx
j5bUV6MRIfNv06rXklhjgX22FUL9p0qilYqJmYQqVYku2Ff431dyDuzv7q0AkMoP
N6+0JUFudG9uaW8gVGVyY2Vpcm8gPHRlcmNlaXJvQGNzLnViYy5jYT6JAjcEEwEI
ACEFAk2SGLsCGwMFCwkIBwMFFQoJCAsFFgIDAQACHgECF4AACgkQ/A2xu81GC95+
mg/+MGbOb7ntM05SO95GNKP8fTQXEnYa6xYIkASkHY7wPfBhYWdYE8kLajWdD1ec
hDIS3nJ/4fVDUrF1ZJM37lIWJZhhQWwLPONCTlxWYTScosYzKkTeQ0PzBBrrsqOF
xTtANvb8N9fE6rxkzl7cT48Ty+B0o+U2BolgtM/lmzdu44c7Wr9QIkCpE3wqMfj2
kw3JUMojOSVDTwavZyPHGa1wibH6R59rQ+WhG6OxiZAXCh8QIEbe6Z50R5Araqc4
x9/yvBN03j7YCSOJ0hDpRS9VWRWiGNEnn20My0mZRPI+VjvnYmR9w6cHWU33LwQ1
/xmpJfhJg989/rDRZlJZ4nYgGZoope+5+HcrOud+wrXBUXht3mMDuZTBoBMM5ey/
Z4Rl1FikNYgD58MDt3bNUyrb+MsUjNuCDPmmdH5CLTG0abxaSjRJ4dR6AZDVqt2x
KVlhzkGEEBRyKRDmphMoC6AG+kIYuJqfnr1zh35QDuBZNRytt22HzDQk8RcMslrd
nx94X/53LRvkdXNXoijhMMz4bt6exBz1GbRcW1Oe3JNeW3btekEgSizTfj5rFxQG
Z8syg28WfQscpY4ffl+YlFALj+8ZqpIzezxa2aB0WqMq8BcFUe7+6NZliDwbOC35
ufMBt8SqCJd/o4Egmz09AZxVfWHlmQfApzKs3ARXnSkLeoK5Ag0ETXvdtwEQAOjS
WZVeJx12YPlaz4ylULBrVpgbcSucTIYkx5sPFc0cJCRseG7AKBOG+Me4SfLma25/
IhcmoEsxCgo9eNM5XMysg3jo7Pui6g8OEw5wNIlKHsrOXk0ITSZpimxltrEMLm5a
eaF95Ne35VNyr285H/AL1qM+Ubwfmpwd1Eu49z39/aY/0IDymdO11Aai8kz2OHED
Rkx/Fe95Xx4JhdkwSYoaE3Tzz8GGU6Rig+MwJZeb9YiFSXHqRgP1+IjI+Ht1APvH
kUKwXfmM1IuGBLE6pw0a6V4mWASW4Dy2+0PE/jR1R5AVxh03sd5eUC/BPQ/wFKH7
0+EryemO5181YLoOv3/RRDHmsxSk9C1BDLboSFbO9Mo5e78xYHkFHonm7iGejYto
DLE9xHixOiyc86jbAkdL7Qjc6NJVNtCvTU58/4aArFBVzfEvWdz1umFcPmJKaCEp
8YI8LalWK5meTbVUDuRWE/mtu8/k3jaCzHhj83GFIoFP6YNWGWlAZJGklhvM5Spr
tvaD78hoaNpuPCh+u0Atuv3uWBHIsczIVxJYbSNvpLu+EN7O4DNnxkFI6GBDhHX+
lGEJdHn5JXmD43c2JVykS9d3XJcWdlyXgwdwRviXz2/kYdP7iAVLhl7T0skk8/A4
xv6p2FRMgixM/PIp1GZu915egPlilrFxYAg1xMGLABEBAAGJAh8EGAEIAAkFAk17
3bcCGwwACgkQ/A2xu81GC97B0w//QT1SS/s1fgJ8IDmbcUrfk3Y5+PvDnmHr8gsJ
8A8hMJ2OLdxa2lrcWXebmiSPConAJlzBUKqYm+WZItjJYqEvRfLjKpzTJuxxSae1
v5v7Y2kUPKgIKmaD9EYl3c3SsyQB8lJ9cBhU6vSdo2L7/nckKSheiEw99MnJBK3B
iyTYikt2NILuInZ0RErZ7xz1jpinCfkvZSYwJ+IiGxCFefYzDFECjj9bWjIE6vz6
4HyrXjtSdXsCcFrL+fSnhWOpdgmUuzdUWBH84mAWM6aXqNzAHOIrp5VatDWpHBPH
bi3M/TIHyjrKnsaYrYU5fmhnTdLig2LMZhKg3vHAqvZfb1XbRgYTSuayGW9sGFG5
AWZDUOuv+E9FAvHGlv7EZJsy+yhQh4cNWlJQJnGEA/HxA2sRZlwGawwjbZ0xF6MG
0Dj8x+9Eu3/nRYO0VYgzAYVKMF5cBfblNnWG8p5Npz6lfr1sLY1W9E/2XWkwvnr5
KDSaDo9iTj+Bhk60vWFDjD69oY1HpAZMGuBd/Z0qy0W/l+ovuMAIU6vH9gHtizdY
uZe/53J8oBHx+zXgPxzCE7mylVCrVgq6MV6NZtEBQSa5PjPrzu12HAnfzHhV/z3Y
o9boBnVcNoOCGxUTqpg1aCDvzH8B8Y6hQKVETA077iYfLMFcqMaMLGwAXxGGJbCy
y/cBuQY=
=oYoG
-----END PGP PUBLIC KEY BLOCK-----
EOF
  run sudo apt-get update
fi

run sudo apt-get -y install dctrl-tools

# needed to run noosfero
packages=$(grep-dctrl -n -s Build-Depends,Depends,Recommends -S -X noosfero debian/control | sed -e 's/([^)]*)//g; s/,\s*/\n/g' | grep -v 'memcached\|debconf\|dbconfig-common\|misc:Depends\|adduser\|mail-transport-agent')
run sudo apt-get -y install $packages
sudo apt-get -y install iceweasel || sudo apt-get -y install firefox

run bundle install
